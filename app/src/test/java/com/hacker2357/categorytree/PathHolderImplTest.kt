package com.hacker2357.categorytree

import com.hacker2357.categorytree.view.component.implementation.PathHolderImpl
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.domain.data.response.CategoryDataRes
import org.junit.Test
import org.mockito.Mockito
import java.lang.Exception

//@RunWith(KotlinTestRunner::class)
class PathHolderImplTest {

    @Test
    fun testGetNewItemPosition() {
        val pathHolder = PathHolderImpl<Long, CategoryDataRes>()

        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        pathHolder.setMediator(mediator)

        pathHolder.setPath(listOf(null, 4L, 2L))

        assert(pathHolder.getNewItemPosition(4) == 2)
        assert(pathHolder.getNewItemPosition(40) == 0)
        assert(pathHolder.getNewItemPosition(null) == 1)
    }

    @Test
    fun testSetActive() {
        val pathHolder = PathHolderImpl<Long, CategoryDataRes>()

        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        pathHolder.setMediator(mediator)

        pathHolder.setPath(listOf(
                null,
                4L,
                2L,
                3L,
                5L,
                6L
        ))

        pathHolder.setActive(2, 8L)

        Mockito.`when`(mediator.getCachedItemById(Mockito.anyLong())).then {
            CategoryDataRes(it.arguments.first() as Long, "", null)
        }

        val item = pathHolder.getItemIdByPosition(2)

        assert(item!! == 8L)

        assert(pathHolder.count() == 3)
    }

    @Test(expected = Exception::class)
    fun testSetActiveException() {
        val pathHolder = PathHolderImpl<Long, CategoryDataRes>()

        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        pathHolder.setMediator(mediator)

        pathHolder.setPath(listOf(
                null,
                4L,
                2L,
                3L,
                5L,
                6L
        ))

        pathHolder.setActive(-1, 8L)
    }

    @Test
    fun testSetPathByData() {
        val pathHolder = PathHolderImpl<Long, CategoryDataRes>()

        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        pathHolder.setMediator(mediator)

        val categoryList = listOf(
                CategoryDataRes(1, "1", null),
                CategoryDataRes(2, "2", 1),
                CategoryDataRes(3, "3", 2),
                CategoryDataRes(4, "4", 3),
                CategoryDataRes(5, "5", 4),
                CategoryDataRes(6, "6", 5)
        )

        Mockito.`when`(mediator.convertToReference(Mockito.nullable(CategoryDataRes::class.java))).then {
            (it.arguments.first() as CategoryDataRes?)?.parent
        }

        Mockito.`when`(mediator.getParentById(any())).then { args ->
            val item = args.arguments.first() as Long?
            val list = ArrayList(categoryList)
            list.asSequence().filter { it.id == item }.firstOrNull()
        }

        pathHolder.setPathByData(4L)

//        println(pathHolder.count())

        assert(pathHolder.count() == 5)
    }

}

private fun <T> any(): T {
    Mockito.any<T>()
    return uninitialized()
}
private fun <T> uninitialized(): T = null as T