package com.hacker2357.categorytree

import android.util.Log
import com.hacker2357.categorytree.view.fragment.tree.CategoryTreeItemFragment
import com.hacker2357.categorytree.view.fragment.tree.DataFragmentItemFragment
import com.hacker2357.categorytree.view.component.implementation.FragmentGeneratorTreeChainItem
import com.hacker2357.categorytree.view.component.interfaces.FragmentGenerator
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.domain.data.response.CategoryDataRes
import org.junit.Test

import org.mockito.Mockito
import org.mockito.exceptions.base.MockitoException


class FragmentGeneratorTreeChainItemTest {

    @Test
    fun testGenerate() {
        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        val next = Mockito.mock(FragmentGenerator::class.java)

        Mockito.`when`(next.generate(Mockito.anyInt())).then {
            throw MockitoException("exception")
//            Mockito.mock(Fragment::class.java)
        }

        Mockito.`when`(mediator.countPath()).thenReturn(4)
        Mockito.`when`(mediator.getChildrenFromCache(Mockito.nullable(Long::class.java))).thenReturn(arrayListOf())

        val fragmentGeneratorTreeChainItem = FragmentGeneratorTreeChainItem(
                mediator,
                CategoryTreeItemFragment::class.java as Class<DataFragmentItemFragment<Long, CategoryDataRes>>,
                next
        )

        fragmentGeneratorTreeChainItem.generate(3)
        try {
            fragmentGeneratorTreeChainItem.generate(4)
            assert(false)
        } catch (e: MockitoException) {
//            e.printStackTrace()
        }

        assert(fragmentGeneratorTreeChainItem.count() == 4)


    }

    @Test
    fun testCount() {
        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        val next = Mockito.mock(FragmentGenerator::class.java)

        Mockito.`when`(next.count()).thenReturn(2)

        Mockito.`when`(mediator.countPath()).thenReturn(4)

        Mockito.`when`(mediator.isNonFinalCache(Mockito.nullable(Long::class.java))).thenReturn(false)

        val fragmentGeneratorTreeChainItem = FragmentGeneratorTreeChainItem(
                mediator,
                CategoryTreeItemFragment::class.java as Class<DataFragmentItemFragment<Long, CategoryDataRes>>,
                next
        )

        assert(fragmentGeneratorTreeChainItem.count() == 6)


    }

    @Test
    fun testCount2() {
        val mediator = Mockito.mock(TreeMediator::class.java) as TreeMediator<Long, CategoryDataRes>

        val next = Mockito.mock(FragmentGenerator::class.java)

        Mockito.`when`(next.count()).thenReturn(2)

        Mockito.`when`(mediator.countPath()).thenReturn(4)

        Mockito.`when`(mediator.isNonFinalCache(Mockito.nullable(Long::class.java))).thenReturn(true)

        val fragmentGeneratorTreeChainItem = FragmentGeneratorTreeChainItem(
                mediator,
                CategoryTreeItemFragment::class.java as Class<DataFragmentItemFragment<Long, CategoryDataRes>>,
                next
        )

        assert(fragmentGeneratorTreeChainItem.count() == 4)


    }
}