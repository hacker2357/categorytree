package com.hacker2357.categorytree.dagger.module.categorytree

import com.hacker2357.categorytree.dagger.scope.CategoryTreeFragmentScope
import com.hacker2357.categorytree.view.fragment.tree.CategoryTreeItemFragment
import com.hacker2357.categorytree.view.fragment.tree.DataFragmentItemFragment
import com.hacker2357.categorytree.view.component.implementation.FragmentGeneratorTreeChainItem
import com.hacker2357.categorytree.view.component.interfaces.FragmentGenerator
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.domain.data.response.CategoryDataRes
import dagger.Module
import dagger.Provides

@Module(includes = [TreeProviderModule::class, CategoryTreeMediatorModule::class])
class FragmentGeneratorModule{

    @Provides
    @CategoryTreeFragmentScope
    fun categoryFragmentGenerator(mediator: TreeMediator<Long, CategoryDataRes>): FragmentGenerator {
        return FragmentGeneratorTreeChainItem(mediator, CategoryTreeItemFragment::class.java as Class<DataFragmentItemFragment<Long, CategoryDataRes>>)
    }
}