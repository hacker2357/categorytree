package com.hacker2357.categorytree.dagger.module.categorytree

import com.hacker2357.categorytree.dagger.scope.CategoryTreeFragmentScope
import com.hacker2357.categorytree.view.component.implementation.*
import com.hacker2357.categorytree.view.component.interfaces.*
import com.hacker2357.domain.data.response.CategoryDataRes
import dagger.Module
import dagger.Provides

@Module
class TreeProviderModule {

    @Provides
    @CategoryTreeFragmentScope
    fun treeItemExtractor(): TreeItemExtractor<Long, CategoryDataRes> {
        return TreeItemExtractorCategory()
    }

    @Provides
    @CategoryTreeFragmentScope
    fun pathHolder(): PathHolder<Long, CategoryDataRes> {
        return PathHolderProxyImpl(PathHolderImpl())
    }

    @Provides
    @CategoryTreeFragmentScope
    fun treeCacheProvider(): TreeCacheProvider<Long, CategoryDataRes> {
        return TreeCacheProviderImpl()
    }

    @Provides
    @CategoryTreeFragmentScope
    fun pathCacher(): PathCacheProvider<Long, CategoryDataRes> {
        return PathCacheProviderImpl()
    }

}