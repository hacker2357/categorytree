package com.hacker2357.categorytree.dagger.component

import com.hacker2357.categorytree.dagger.module.categorytree.CategoryTreeMediatorModule
import com.hacker2357.categorytree.dagger.module.categorytree.FragmentGeneratorModule
import com.hacker2357.categorytree.dagger.module.categorytree.FragmentManagerModule
import com.hacker2357.categorytree.dagger.scope.CategoryTreeFragmentScope
import com.hacker2357.categorytree.view.fragment.CategoryTreeFragment
import dagger.Component

@Component(dependencies = [MainComponent::class], modules = [FragmentGeneratorModule::class, FragmentManagerModule::class, CategoryTreeMediatorModule::class])
@CategoryTreeFragmentScope
interface CategoryTreeViewComponent {
    fun inject(fragment: CategoryTreeFragment)
}