package com.hacker2357.categorytree.dagger.component

import android.content.Context
import com.hacker2357.categorytree.dagger.module.main.AppModule
import com.hacker2357.categorytree.dagger.module.main.FragmentInjectorModule
import com.hacker2357.categorytree.view.activity.MainActivity
import com.hacker2357.categorytree.view.component.interfaces.FragmentInjector
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, FragmentInjectorModule::class])
@Singleton
interface MainComponent {
    fun context(): Context
    fun inject(activity: MainActivity)
    fun injector(): FragmentInjector
}