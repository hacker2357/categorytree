package com.hacker2357.categorytree.dagger.scope

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class CategoryListFragmentScope