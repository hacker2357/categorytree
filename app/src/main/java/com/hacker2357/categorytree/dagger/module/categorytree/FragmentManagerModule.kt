package com.hacker2357.categorytree.dagger.module.categorytree

import android.support.v4.app.FragmentManager
import com.hacker2357.categorytree.dagger.scope.CategoryTreeFragmentScope
import dagger.Module
import dagger.Provides

@Module
class FragmentManagerModule(private val fragmentManager: FragmentManager) {
    @Provides
    @CategoryTreeFragmentScope
    fun fragmentManager(): FragmentManager = fragmentManager
}