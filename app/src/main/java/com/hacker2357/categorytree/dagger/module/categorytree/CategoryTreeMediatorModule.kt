package com.hacker2357.categorytree.dagger.module.categorytree

import com.hacker2357.categorytree.dagger.scope.CategoryTreeFragmentScope
import com.hacker2357.categorytree.view.component.implementation.TreeMediatorImpl
import com.hacker2357.categorytree.view.component.interfaces.*
import com.hacker2357.domain.data.response.CategoryDataRes
import dagger.Module
import dagger.Provides

@Module(includes = [TreeProviderModule::class])
class CategoryTreeMediatorModule {
    @Provides
    @CategoryTreeFragmentScope
    fun treeMediator(pathHolder: PathHolder<Long, CategoryDataRes>,
                     treeCacheProvider: TreeCacheProvider<Long, CategoryDataRes>,
                     treeItemExtractor: TreeItemExtractor<Long, CategoryDataRes>,
                     pathCacheProvider: PathCacheProvider<Long, CategoryDataRes>
    ): TreeMediator<Long, CategoryDataRes> {
        return TreeMediatorImpl(pathHolder, treeCacheProvider, treeItemExtractor, pathCacheProvider)
    }
}