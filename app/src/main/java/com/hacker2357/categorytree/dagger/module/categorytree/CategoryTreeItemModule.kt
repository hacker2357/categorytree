package com.hacker2357.categorytree.dagger.module.categorytree

import com.hacker2357.categorytree.view.component.implementation.RecyclerViewScrollOffsetComputerImpl
import com.hacker2357.categorytree.view.component.interfaces.RecyclerViewScrollOffsetComputer
import dagger.Module
import dagger.Provides

@Module
class CategoryTreeItemModule {
    @Provides
    fun recyclerViewOffsetComputer(): RecyclerViewScrollOffsetComputer {
        return RecyclerViewScrollOffsetComputerImpl()
    }
}