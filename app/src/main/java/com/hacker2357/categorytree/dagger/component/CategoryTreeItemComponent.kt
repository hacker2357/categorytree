package com.hacker2357.categorytree.dagger.component

import com.hacker2357.categorytree.dagger.module.categorytree.CategoryTreeItemModule
import com.hacker2357.categorytree.dagger.scope.CategoryListFragmentScope
import com.hacker2357.categorytree.view.fragment.tree.CategoryTreeItemFragment
import dagger.Component

@Component(dependencies = [MainComponent::class], modules = [CategoryTreeItemModule::class])
@CategoryListFragmentScope
interface CategoryTreeItemComponent {
    fun inject(fragment: CategoryTreeItemFragment)
}