package com.hacker2357.categorytree.dagger.module.main

import com.hacker2357.categorytree.view.component.implementation.GeneralFragmentInjector
import com.hacker2357.categorytree.view.component.interfaces.FragmentInjector
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FragmentInjectorModule {

    @Provides
    @Singleton
    fun fragmentInjector(): FragmentInjector = GeneralFragmentInjector()
}