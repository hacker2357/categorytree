package com.hacker2357.categorytree.view.interfaces.presenter

interface BasePresenter {
    fun init()
    fun destroy()
}