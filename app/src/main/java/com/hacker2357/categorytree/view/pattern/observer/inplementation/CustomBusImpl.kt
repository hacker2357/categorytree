package com.hacker2357.categorytree.view.pattern.observer.inplementation

import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBus
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBusListener

class CustomBusImpl<RequestDataType, ReturnDataType>(
        requestCallback: CustomBus.Callback<RequestDataType?>? = null
): CustomBus<RequestDataType, ReturnDataType> {

    private var customBusListener: MutableSet<CustomBusListener<RequestDataType, ReturnDataType>> = hashSetOf()
    private var callback: CustomBus.Callback<RequestDataType?>? = null

    init {
        callback = requestCallback
    }


    override fun setCallback(callback: CustomBus.Callback<RequestDataType?>) {
        this.callback = callback
    }

    override fun emit(request: RequestDataType?, data: ReturnDataType?) {
        val listeners = HashSet(customBusListener)
        listeners.forEach {
            it.onEvent(request, data)
        }
    }

    override fun subscribe(customBusListener: CustomBusListener<RequestDataType, ReturnDataType>): CustomBus.Subscription {
        this.customBusListener.add(customBusListener)
        return SubscriptionImpl(customBusListener)
    }

    override fun unsubscribe(listener: CustomBusListener<RequestDataType, ReturnDataType>) {
        if (this.customBusListener.contains(listener)) {
            this.customBusListener.remove(listener)
        }
    }

    override fun request(data: RequestDataType?) {
        callback?.invoke(data)
    }

    override fun destroy() {
        customBusListener.clear()
        callback = null
    }

    inner class SubscriptionImpl(private val customBusListener: CustomBusListener<RequestDataType, ReturnDataType>): CustomBus.Subscription {
        private var isDisposed = false

        override fun isDisposed(): Boolean {
            return isDisposed
        }

        override fun dispose() {
            if (!isDisposed()) {
                unsubscribe(customBusListener)
                isDisposed = true
            }
        }

    }
}