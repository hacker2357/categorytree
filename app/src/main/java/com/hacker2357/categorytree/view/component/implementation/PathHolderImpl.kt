package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.PathHolder
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import javax.inject.Inject

class PathHolderImpl<RequestDataType: Comparable<RequestDataType>, ReturnDataType> @Inject constructor() : PathHolder<RequestDataType, ReturnDataType> {

    private val _path: MutableList<RequestDataType?> = arrayListOf(null)

    private lateinit var _mediator: TreeMediator<RequestDataType, ReturnDataType>


    override fun setMediator(mediator: TreeMediator<RequestDataType, ReturnDataType>) {
        _mediator = mediator
    }

    override fun getNewItemPosition(id: RequestDataType?): Int {

        val indexOf = _path.indexOf(id)
        return indexOf + 1
    }

    override fun setPath(path: List<RequestDataType?>) {
        synchronized(_path) {
            _path.clear()
            _path.addAll(path)
        }
    }

    override fun setActive(x: Int, activeItem: RequestDataType?) {

        if (x == -1) {
            throw Exception("Invalid x exception")
        }

        if (activeItem === null && x > 0) {
            return
        }

        synchronized(_path) {
            when {
                _path.size <= x -> _path.add(activeItem)
                _path.size > x -> _path[x] = activeItem
            }

            val newPath = ArrayList(_path.subList(0, x + 1))
            _path.clear()
            _path.addAll(newPath)
        }
    }

    override fun getItemIdByPosition(position: Int): RequestDataType? {
        if (_path.size <= position) {
            return null
        }
        return _path[position]
//        return _mediator.getCachedItemById(_path[x])
    }

    override fun count(): Int {
        return _path.size
    }

    override fun last(): RequestDataType? {
        return _path.last()
    }

    override fun setPathByData(item: RequestDataType) {
        synchronized(_path) {
            var cursorItem: RequestDataType? = item
            val path = arrayListOf<RequestDataType?>()

            while (cursorItem !== null) {
                path.add(0, cursorItem)

                cursorItem = _mediator.convertToReference(_mediator.getParentById(cursorItem))

            }

            if (!_path.contains(path.last())) {
                _path.clear()
                _path.add(null)
                _path.addAll(path)
            }

//            Log.e("setPathByData", _path.size.toString())

//            _mediator.clonePathCache()
            _mediator.notifyDataSetChangedMediator()
        }
    }

    override fun getPositionByItem(item: RequestDataType?): Int {
        return _path.indexOf(item)
    }

    override fun getPath(): List<RequestDataType?> {
        return _path
    }

}