package com.hacker2357.categorytree.view.activity

import android.os.Bundle
import com.hacker2357.categorytree.App
import com.hacker2357.categorytree.R
import com.hacker2357.categorytree.view.fragment.CategoryTreeFragment
import com.hacker2357.categorytree.view.component.interfaces.FragmentInjector
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    protected lateinit var fragmentInjector: FragmentInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        App.mainComponent().inject(this)

        fragmentInjector.inject(R.id.fragment_replace, supportFragmentManager, CategoryTreeFragment())
    }
}
