package com.hacker2357.categorytree.view.component.interfaces

import android.support.v7.widget.RecyclerView

interface RecyclerViewScrollOffsetComputer {
    fun compute(recyclerView: RecyclerView): Int
}