package com.hacker2357.categorytree.view.component.interfaces

import android.support.v7.widget.RecyclerView

interface ScrollListener {
    fun scrollChanged(recyclerView: RecyclerView)
}