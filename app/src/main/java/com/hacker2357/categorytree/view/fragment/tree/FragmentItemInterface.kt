package com.hacker2357.categorytree.view.fragment.tree

interface FragmentItemInterface<ItemType> {
    fun getCurrentItem(): ItemType?
    fun onActivate()
}