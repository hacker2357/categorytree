package com.hacker2357.categorytree.view.component.implementation

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.hacker2357.categorytree.R
import com.hacker2357.categorytree.view.component.interfaces.FragmentInjector
import javax.inject.Inject

class GeneralFragmentInjector @Inject constructor(): FragmentInjector {
    override fun inject(@IdRes id: Int, fragmentManager: FragmentManager, fragment: Fragment): Int {
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_replace, fragment)
        return transaction.commit()
    }
}