package com.hacker2357.categorytree.view.component.interfaces

interface PathHolder<DataType: Comparable<DataType>, ReturnItemDataType> : TreeColleague<DataType, ReturnItemDataType> {
    fun setPath(path: List<DataType?>)
    fun setActive(x: Int, activeItem: DataType?)
    fun getItemIdByPosition(position: Int): DataType?
    fun count(): Int
    fun getNewItemPosition(id: DataType?): Int
    fun last(): DataType?
    fun setPathByData(item: DataType)
    fun getPositionByItem(item: DataType?): Int
    fun getPath(): List<DataType?>
}