package com.hacker2357.categorytree.view.component.implementation

import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import com.hacker2357.categorytree.view.fragment.tree.FragmentItemInterface
import java.util.*

class PageSelectedDelayer(private val viewPager: ViewPager) : ViewPager.OnPageChangeListener {

    private var lastScrollX = 0
    private var timer = Timer()
    private val handler = Handler(Looper.getMainLooper())


    override fun onPageScrollStateChanged(state: Int) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            lastScrollX = viewPager.scrollX
        }
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

    }

    override fun onPageSelected(position: Int) {
        timer.cancel()
        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post {
                    if (lastScrollX != viewPager.scrollX) {
                        onPageSelected(position)
                    } else {
                        runPageSelected(position)
                    }
                }
            }

        }, 150L)
    }

    private fun runPageSelected(position: Int) {
        val adapter = viewPager.adapter!!
        if (adapter.count > position) {
            val fragment = adapter.instantiateItem(viewPager, position) as Fragment
            if (fragment is FragmentItemInterface<*> && fragment.isAdded) {
                fragment.onActivate()
            }
        }
    }
}