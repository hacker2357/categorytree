package com.hacker2357.categorytree.view.component.interfaces

interface Updatable {
    fun update()
}