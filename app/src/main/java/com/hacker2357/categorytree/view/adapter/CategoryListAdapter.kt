package com.hacker2357.categorytree.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hacker2357.categorytree.R
import com.hacker2357.domain.data.response.CategoryDataRes
import kotlinx.android.synthetic.main.fragment_list_category.view.*
import javax.inject.Inject

class CategoryListAdapter @Inject constructor() : RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {

    private val categoryList: MutableList<CategoryDataRes> = ArrayList()

    fun setData(list: List<CategoryDataRes>) {
        categoryList.clear()
        categoryList.addAll(list)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): CategoryDataRes {
        return categoryList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_category, parent, false)
        view.layoutParams.height = parent.height / 9
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoryList.size + 8
    }

    private fun isOutOfRange(position: Int): Boolean {
        return position < 4 || position >= categoryList.size + 4
    }

    private fun setViewHolderVisibility(holder: ViewHolder, position: Int) {
        if (isOutOfRange(position)) {
            holder.itemView.categoryTitle.visibility = View.GONE
        } else {
            holder.itemView.categoryTitle.visibility = View.VISIBLE
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)

        setViewHolderVisibility(holder, holder.adapterPosition)
        holder.showProgressBar(false)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        setViewHolderVisibility(holder, holder.adapterPosition)

        if (isOutOfRange(position)) {
            return
        }

        holder.itemView.categoryTitle.text = categoryList[position - 4].name
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun showProgressBar(show: Boolean) {
            itemView.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }
}