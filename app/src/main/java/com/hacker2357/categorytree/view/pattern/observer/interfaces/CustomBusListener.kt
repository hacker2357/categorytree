package com.hacker2357.categorytree.view.pattern.observer.interfaces

interface CustomBusListener<RequestDataType, DataType> {
    fun onEvent(request: RequestDataType?, data: DataType?)
    fun onFailed(e: Throwable)
}