package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.*
import com.hacker2357.categorytree.view.pattern.observer.inplementation.CustomBusImpl
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBus
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBusListener

class TreeMediatorImpl<RequestDataType: Comparable<RequestDataType>, ReturnItemDataType>(
        private val pathHolder: PathHolder<RequestDataType, ReturnItemDataType>,
        private val treeCacheProvider: TreeCacheProvider<RequestDataType, ReturnItemDataType>,
        private val treeItemExtractor: TreeItemExtractor<RequestDataType, ReturnItemDataType>,
        private val pathCacheProvider: PathCacheProvider<RequestDataType, ReturnItemDataType>
) : TreeMediator<RequestDataType, ReturnItemDataType> {

    private var _updatable: Updatable? = null

    private val listenersBus: CustomBus<RequestDataType?, List<ReturnItemDataType>?> = CustomBusImpl()

    init {
        pathHolder.setMediator(this)
        treeCacheProvider.setMediator(this)
        treeItemExtractor.setMediator(this)
        pathCacheProvider.setMediator(this)

        listenersBus.subscribe(object : CustomBusListener<RequestDataType?, List<ReturnItemDataType>?> {
            override fun onFailed(e: Throwable) {

            }

            override fun onEvent(request: RequestDataType?, data: List<ReturnItemDataType>?) {
                if (data!!.isNotEmpty()) {
                    if (!treeCacheProvider.isNonFinal(request)) {
                        treeCacheProvider.addList(data)
                        if (request === null) {
                            pathCacheProvider.clear()
                        }
                    }
                }

                _updatable?.update()
            }

        })
    }

    override fun setLoadListener(loadListener: CustomBus.Callback<RequestDataType?>) {
        listenersBus.setCallback(loadListener)
    }

    override fun notifyDataSetChangedMediator() {
        _updatable?.update()
//        treeCacheProvider.updatePath()
    }

    override fun setUpdatable(updatable: Updatable) {
        _updatable = updatable
    }

    override fun setPath(path: List<RequestDataType>) {
        pathHolder.setPath(path)
    }

    override fun setActiveItemPath(x: Int, activeItem: RequestDataType?) {
        pathHolder.setActive(x, activeItem)
    }

    override fun getItemByPositionInPath(position: Int): RequestDataType? {
        return pathHolder.getItemIdByPosition(position)
    }

    override fun countPath(): Int {
        return pathHolder.count()
    }

    override fun getNewItemPositionInPath(data: RequestDataType?): Int {
        return pathHolder.getNewItemPosition(data)
    }

    override fun getChildrenFromCache(item: RequestDataType?): List<ReturnItemDataType>? {
        return treeCacheProvider.getChildren(item)
    }

    override fun getPeersFromCache(item: RequestDataType): List<ReturnItemDataType> {
        return treeCacheProvider.getPeers(item)
    }

    override fun addListToCache(newList: List<ReturnItemDataType>) {
        treeCacheProvider.addList(newList)
    }

    override fun requestData(data: RequestDataType?) {
        if (!treeCacheProvider.isNonFinal(data)) {
            listenersBus.request(data)
        }
    }

    override fun emitBus(request: RequestDataType?, data: List<ReturnItemDataType>?) {
        listenersBus.emit(request, data)
    }

    override fun subscribeBus(observer: CustomBusListener<RequestDataType?, List<ReturnItemDataType>?>) {
        listenersBus.subscribe(observer)
    }

    override fun unsubscribeBus(observer: CustomBusListener<RequestDataType?, List<ReturnItemDataType>?>) {
        listenersBus.unsubscribe(observer)
    }

    override fun destroyBus() {
        listenersBus.destroy()
    }

    override fun extractId(item: ReturnItemDataType): RequestDataType {
        return treeItemExtractor.getIdRequest(item)
    }

    override fun extractParent(item: ReturnItemDataType): RequestDataType? {
        return treeItemExtractor.getParentRequest(item)
    }

    override fun extractRequestParent(item: RequestDataType): RequestDataType? {
        return treeItemExtractor.getParent(item)
    }

    override fun getCachedItemById(itemId: RequestDataType?): ReturnItemDataType? {
        return treeCacheProvider.getById(itemId)
    }

    override fun convertToReference(item: ReturnItemDataType?): RequestDataType? {
        return treeItemExtractor.convertToReference(item)
    }

    override fun getParentById(item: RequestDataType): ReturnItemDataType? {
        return treeCacheProvider.getParent(item)
    }

    override fun getPositionByItemInPath(data: RequestDataType?): Int {
        return pathHolder.getPositionByItem(data)
    }

    override fun isNonFinalCache(data: RequestDataType?): Boolean {
        return treeCacheProvider.isNonFinal(data)
    }

    override fun setPathByData(item: RequestDataType) {
        pathHolder.setPathByData(item)
    }

    override fun getPath(): List<RequestDataType?> {
        return pathHolder.getPath()
    }


    override fun clonePathCache() {
        pathCacheProvider.clonePath()
    }

    override fun isDifferentPathCache(position: Int): Boolean {
        return pathCacheProvider.isDifferent(position)
    }

    override fun clearPathCache() {
        pathCacheProvider.clear()
    }

    override fun navigateTo(item: RequestDataType) {
        pathHolder.setPathByData(item)
        requestData(item)
        notifyDataSetChangedMediator()
    }

}