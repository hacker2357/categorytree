package com.hacker2357.categorytree.view.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hacker2357.categorytree.App
import com.hacker2357.categorytree.R
import com.hacker2357.categorytree.dagger.component.DaggerCategoryTreeViewComponent
import com.hacker2357.categorytree.dagger.module.categorytree.FragmentManagerModule
import com.hacker2357.categorytree.view.adapter.TreeAdapter
import com.hacker2357.categorytree.view.component.implementation.PageSelectedDelayer
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.categorytree.view.interfaces.view.TreeView
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBus
import com.hacker2357.domain.data.response.CategoryDataRes
import kotlinx.android.synthetic.main.fragment_tree.*
import javax.inject.Inject

class CategoryTreeFragment: BaseFragment(), TreeView {

    @Inject
    protected lateinit var treeAdapter: TreeAdapter
    @Inject
    protected lateinit var mediator: TreeMediator<Long, CategoryDataRes>

    override fun setCategories(categoryId: Long?, categories: List<CategoryDataRes>) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tree, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var counter = 0

        DaggerCategoryTreeViewComponent.builder()
                .mainComponent(App.mainComponent())
                .fragmentManagerModule(FragmentManagerModule(childFragmentManager))
                .build()
                .inject(this)

        mediator.setLoadListener(object : CustomBus.Callback<Long?> {
            override fun invoke(id: Long?) {
//              Временный костыль, чтобы работало без интернета, здесь должен делаться запрос в презентер
                var stringPath = ""
                mediator.getPath().forEach { item ->
                    val parent = if (item === null) {
                        null
                    } else {
                        mediator.getParentById(item)?.id
                    }

                    stringPath += mediator.getChildrenFromCache(parent)?.map { it.id }?.indexOf(item)?.toString() + " -> "
                }

                Handler().postDelayed({


                    mediator.emitBus(id, listOf(
                            CategoryDataRes((++counter).toLong(), stringPath + "0 [$counter]", id),
                            CategoryDataRes((++counter).toLong(), stringPath + "1 [$counter]", id),
                            CategoryDataRes((++counter).toLong(), stringPath + "2 [$counter]", id),
                            CategoryDataRes((++counter).toLong(), stringPath + "3 [$counter]", id)
                    ))
                }, 500L)
            }

        })

        treePager.adapter = treeAdapter

        val treePagerListener = PageSelectedDelayer(treePager)
        treePager.addOnPageChangeListener(treePagerListener)

        treePager.offscreenPageLimit = 0

        treePager.post {
            treePagerListener.onPageSelected(0)
        }

        mediator.setUpdatable(treeAdapter)

        mediator.requestData(null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroyView() {
        mediator.destroyBus()
        super.onDestroyView()
    }
}
