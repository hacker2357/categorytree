package com.hacker2357.categorytree.view.component.interfaces

interface PathCacheProvider<RequestDataType: Comparable<RequestDataType>, ReturnDataType> : TreeColleague<RequestDataType, ReturnDataType> {
    fun clonePath()
    fun isDifferent(position: Int): Boolean
    fun clear()

}