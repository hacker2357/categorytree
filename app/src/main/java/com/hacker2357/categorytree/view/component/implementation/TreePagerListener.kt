package com.hacker2357.categorytree.view.component.implementation

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import com.hacker2357.categorytree.view.fragment.tree.FragmentItemInterface

class TreePagerListener(private val treePager: ViewPager, private val treeAdapter: PagerAdapter) : ViewPager.OnPageChangeListener {
    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

    }

    override fun onPageSelected(position: Int) {
        val fragment = treeAdapter.instantiateItem(treePager, position)
        if (fragment is FragmentItemInterface<*>) {
            fragment.onActivate()
        }
    }

    override fun onPageScrollStateChanged(p0: Int) {
    }
}