package com.hacker2357.categorytree.view.adapter

import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.hacker2357.categorytree.view.component.interfaces.RecyclerViewScrollOffsetComputer
import com.hacker2357.categorytree.view.component.interfaces.ScrollListener
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.domain.data.response.CategoryDataRes

class CategoryMediator2ScrollListenerAdapter(
        private val adapter: CategoryListAdapter,
        private val mediator: TreeMediator<Long, CategoryDataRes>,
        private val recyclerViewScrollOffsetComputer: RecyclerViewScrollOffsetComputer) : ScrollListener {

    override fun scrollChanged(recyclerView: RecyclerView) {
        val position = recyclerViewScrollOffsetComputer.compute(recyclerView)
        Log.e("Position", position.toString())
        if (adapter.itemCount - 8 > position) {

            val pathTargetItem = mediator.extractId(adapter.getItem(position))

            mediator.navigateTo(pathTargetItem)

            Log.e("TreeAdapter", "pathCount ${mediator.countPath()}")

            if (!mediator.isNonFinalCache(pathTargetItem)) {
                val holder = recyclerView.findViewHolderForAdapterPosition(position + 4) as CategoryListAdapter.ViewHolder
                holder.showProgressBar(true)
            }
        }
    }


}