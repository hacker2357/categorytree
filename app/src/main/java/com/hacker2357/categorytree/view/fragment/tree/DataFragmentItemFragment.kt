package com.hacker2357.categorytree.view.fragment.tree

import com.hacker2357.categorytree.view.fragment.BaseFragment
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class DataFragmentItemFragment<RequestDataType, ReturnItemDataType>: BaseFragment(), FragmentItemInterface<RequestDataType> {
    private val compositeDisposable = CompositeDisposable()

    abstract fun setInitialData(currentItem: RequestDataType?, mediator: TreeMediator<RequestDataType, ReturnItemDataType>, activeItem: RequestDataType?)

    fun setSubscription(subscription: Disposable) {
        compositeDisposable.addAll(subscription)
    }
}