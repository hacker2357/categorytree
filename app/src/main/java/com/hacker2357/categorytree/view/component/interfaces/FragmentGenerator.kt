package com.hacker2357.categorytree.view.component.interfaces

import android.support.v4.app.Fragment

interface FragmentGenerator {
    fun generate(position: Int): Fragment
    fun isChanged(fragment: Fragment): Boolean
    fun count(): Int
    fun updatePath()
}