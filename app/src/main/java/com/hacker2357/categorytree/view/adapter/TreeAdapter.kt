package com.hacker2357.categorytree.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.ViewGroup
import com.hacker2357.categorytree.view.component.interfaces.FragmentGenerator
import com.hacker2357.categorytree.view.component.interfaces.Updatable
import javax.inject.Inject

class TreeAdapter @Inject constructor(fm: FragmentManager, private val fragmentGenerator: FragmentGenerator)
    : FragmentStatePagerAdapter(fm), Updatable {

    override fun update() {
        Log.e("TreeAdapter", "update")
        notifyDataSetChanged()
    }


    override fun getItem(position: Int): Fragment {
        return fragmentGenerator.generate(position)
    }

    override fun getCount(): Int {
        val count = fragmentGenerator.count()
        Log.e("TreeAdapter", "getCount $count")
        return count
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        /*if (fragmentGenerator.isChanged(fragment)) {
            if (!fragment.isAdded) {
                fragment.setInitialSavedState(null)
            }
        }*/

        if (position == count - 1) {
            fragmentGenerator.updatePath()
        }

        return fragment
    }

    override fun getItemPosition(fragment: Any): Int {

//        val position = super.getItemPosition(`object`)


        Log.e("getItemPosition", "getItemPosition")

        if (fragmentGenerator.isChanged(fragment as Fragment)) {
            Log.e("getItemPosition", "isChanged")
            return PagerAdapter.POSITION_NONE
        }
        return super.getItemPosition(fragment)
    }
}