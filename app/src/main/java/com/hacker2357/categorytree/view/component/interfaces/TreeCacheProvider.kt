package com.hacker2357.categorytree.view.component.interfaces

interface TreeCacheProvider<RequestDataType : Comparable<RequestDataType>, ReturnItemType> : TreeColleague<RequestDataType, ReturnItemType> {
    fun getChildren(item: RequestDataType?): List<ReturnItemType>?
    fun getPeers(item: RequestDataType): List<ReturnItemType>
    fun addList(newList: List<ReturnItemType>)
    fun getById(itemId: RequestDataType?): ReturnItemType?
    fun getParent(item: RequestDataType): ReturnItemType?
    fun isNonFinal(item: RequestDataType?): Boolean
}