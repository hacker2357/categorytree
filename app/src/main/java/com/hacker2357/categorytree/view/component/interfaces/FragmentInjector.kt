package com.hacker2357.categorytree.view.component.interfaces

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

interface FragmentInjector {
    fun inject(@IdRes id: Int, fragmentManager: FragmentManager, fragment: Fragment): Int
}