package com.hacker2357.categorytree.view.component.interfaces

interface TreeItemExtractor<DataType: Comparable<DataType>, ReturnDataType> : TreeColleague<DataType, ReturnDataType> {
    fun getParent(item: DataType?): DataType?

    fun getIdRequest(item: ReturnDataType): DataType
    fun getParentRequest(item: ReturnDataType?): DataType?
    fun convertToReference(item: ReturnDataType?): DataType?

}