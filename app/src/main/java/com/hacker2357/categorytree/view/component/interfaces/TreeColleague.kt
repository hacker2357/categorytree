package com.hacker2357.categorytree.view.component.interfaces

interface TreeColleague<DataType: Comparable<DataType>, ReturnItemType> {
    fun setMediator(mediator: TreeMediator<DataType, ReturnItemType>)
}