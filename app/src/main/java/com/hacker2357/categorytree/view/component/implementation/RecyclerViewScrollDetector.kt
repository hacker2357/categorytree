package com.hacker2357.categorytree.view.component.implementation

import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import com.hacker2357.categorytree.view.component.interfaces.ScrollListener
import java.util.*

class RecyclerViewScrollDetector(private val callback: ScrollListener) : RecyclerView.OnScrollListener() {
    private var lastScrollY = 0
    private var timer = Timer()
    private val handler = Handler(Looper.getMainLooper())

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            delayedScrollStopChecker(recyclerView)
        }
    }

    private fun delayedScrollStopChecker(recyclerView: RecyclerView) {
        timer.cancel()
        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post {
                    if (lastScrollY != recyclerView.scrollY) {
                        delayedScrollStopChecker(recyclerView)
                    } else {
                        callback.scrollChanged(recyclerView)
                    }
                }
            }

        }, 150L)

    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        lastScrollY = recyclerView.scrollY
    }
}