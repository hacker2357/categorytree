package com.hacker2357.categorytree.view.component.interfaces

import com.hacker2357.categorytree.Mockable
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBus
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBusListener

@Mockable
interface TreeMediator<DataType, ReturnDataTypeItem> {

    fun setLoadListener(loadListener: CustomBus.Callback<DataType?>)

    fun setPath(path: List<DataType>)
    fun setActiveItemPath(x: Int, activeItem: DataType?)
    fun getItemByPositionInPath(position: Int): DataType?
    fun countPath(): Int
    fun getNewItemPositionInPath(data: DataType?): Int
    fun getPositionByItemInPath(data: DataType?): Int
    fun setPathByData(item: DataType)



    fun getChildrenFromCache(item: DataType?): List<ReturnDataTypeItem>?
    fun getPeersFromCache(item: DataType): List<ReturnDataTypeItem>
    fun addListToCache(newList: List<ReturnDataTypeItem>)
    fun isNonFinalCache(data: DataType?): Boolean


    fun requestData(data: DataType? = null)
    fun emitBus(request: DataType?, data: List<ReturnDataTypeItem>?)
    fun subscribeBus(observer: CustomBusListener<DataType?, List<ReturnDataTypeItem>?>)
    fun unsubscribeBus(observer: CustomBusListener<DataType?, List<ReturnDataTypeItem>?>)
    fun destroyBus()


    fun extractId(item: ReturnDataTypeItem): DataType
    fun extractParent(item: ReturnDataTypeItem): DataType?

//    fun extractRequestId(item: DataType?): DataType?
    fun extractRequestParent(item: DataType): DataType?

    fun getCachedItemById(itemId: DataType?): ReturnDataTypeItem?
    fun convertToReference(item: ReturnDataTypeItem?): DataType?
    fun getParentById(item: DataType): ReturnDataTypeItem?
    fun notifyDataSetChangedMediator()
    fun setUpdatable(updatable: Updatable)
    fun getPath(): List<DataType?>

    fun clonePathCache()
    fun isDifferentPathCache(position: Int): Boolean
    fun clearPathCache()

    fun navigateTo(item: DataType)
}