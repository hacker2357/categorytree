package com.hacker2357.categorytree.view.activity

import android.support.v7.app.AppCompatActivity
import com.arellomobile.mvp.MvpAppCompatActivity

abstract class BaseActivity : MvpAppCompatActivity() {

}