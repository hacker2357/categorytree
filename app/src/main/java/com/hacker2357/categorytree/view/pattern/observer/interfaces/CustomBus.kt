package com.hacker2357.categorytree.view.pattern.observer.interfaces

interface CustomBus<RequestDataType, ReturnDataType> {
    fun request(data: RequestDataType? = null)
    fun emit(request: RequestDataType?, data: ReturnDataType?)
    fun subscribe(customBusListener: CustomBusListener<RequestDataType, ReturnDataType>): CustomBus.Subscription
    fun unsubscribe(listener: CustomBusListener<RequestDataType, ReturnDataType>)
    fun destroy()
    fun setCallback(callback: Callback<RequestDataType?>)

    interface Subscription {
        fun isDisposed(): Boolean
        fun dispose()
    }

    interface Callback<InnerRequestDataType> {
        fun invoke(id: InnerRequestDataType? = null)
    }
}