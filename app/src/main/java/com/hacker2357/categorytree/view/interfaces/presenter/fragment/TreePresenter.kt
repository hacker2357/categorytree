package com.hacker2357.categorytree.view.interfaces.presenter.fragment

import com.hacker2357.categorytree.view.interfaces.presenter.BasePresenter

interface TreePresenter : BasePresenter {
    override fun init() {
        requestChildren()
    }

    fun requestChildren(categoryId: Long? = null)

}