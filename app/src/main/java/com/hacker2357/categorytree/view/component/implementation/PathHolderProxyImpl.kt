package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.PathHolder
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator

class PathHolderProxyImpl<RequestDataType: Comparable<RequestDataType>, ReturnDataType>(private val pathHolder: PathHolder<RequestDataType, ReturnDataType>) : PathHolder<RequestDataType, ReturnDataType> {

    private lateinit var _mediator: TreeMediator<RequestDataType, ReturnDataType>

    override fun setMediator(mediator: TreeMediator<RequestDataType, ReturnDataType>) {
        _mediator = mediator
        pathHolder.setMediator(mediator)
    }

    override fun setPath(path: List<RequestDataType?>) {
        pathHolder.setPath(path)
    }

    override fun setActive(x: Int, activeItem: RequestDataType?) {
        pathHolder.setActive(x, activeItem)
    }

    override fun getItemIdByPosition(position: Int): RequestDataType? {
        return pathHolder.getItemIdByPosition(position)
    }

    override fun count(): Int {

        val last = pathHolder.last()
        if (last === null) {
            return pathHolder.count()
        }

        if (_mediator.isNonFinalCache(last)) {
            return pathHolder.count()
        }

        return pathHolder.count() - 1
    }

    override fun getNewItemPosition(id: RequestDataType?): Int {
        return pathHolder.getNewItemPosition(id)
    }

    override fun last(): RequestDataType? {
        return pathHolder.last()
    }

    override fun setPathByData(item: RequestDataType) {
        pathHolder.setPathByData(item)
    }

    override fun getPositionByItem(item: RequestDataType?): Int {
        return pathHolder.getPositionByItem(item)
    }

    override fun getPath(): List<RequestDataType?> {
        return pathHolder.getPath()
    }
}