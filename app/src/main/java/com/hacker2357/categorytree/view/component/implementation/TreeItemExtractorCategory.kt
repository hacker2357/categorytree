package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.TreeItemExtractor
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.domain.data.response.CategoryDataRes

open class TreeItemExtractorCategory : TreeItemExtractor<Long, CategoryDataRes> {

    private lateinit var _mediator: TreeMediator<Long, CategoryDataRes>

    override fun setMediator(mediator: TreeMediator<Long, CategoryDataRes>) {
        _mediator = mediator
    }

    override fun getParent(item: Long?): Long? {

        if (item === null) {
            return null
        }

        return _mediator.getParentById(item)?.parent
    }

    override fun getIdRequest(item: CategoryDataRes): Long {
        return item.id
    }

    override fun getParentRequest(item: CategoryDataRes?): Long? {
        return item?.parent
    }

    override fun convertToReference(item: CategoryDataRes?): Long? {
        return item?.id
    }
}