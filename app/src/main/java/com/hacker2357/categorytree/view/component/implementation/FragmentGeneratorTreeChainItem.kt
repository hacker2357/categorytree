package com.hacker2357.categorytree.view.component.implementation

import android.support.v4.app.Fragment
import com.hacker2357.categorytree.view.fragment.tree.DataFragmentItemFragment
import com.hacker2357.categorytree.view.component.interfaces.FragmentGenerator
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator

class FragmentGeneratorTreeChainItem<RequestDataType : Comparable<RequestDataType>, ReturnItemDataType>(
        private val mediator: TreeMediator<RequestDataType, ReturnItemDataType>,
        private val clazz: Class<DataFragmentItemFragment<RequestDataType, ReturnItemDataType>>,
        private val next: FragmentGenerator? = null
) : FragmentGenerator {

    override fun updatePath() {
        mediator.clonePathCache()
        next?.updatePath()
    }

    override fun isChanged(fragment: Fragment): Boolean {

        if (!clazz.isInstance(fragment)) {
            return next!!.isChanged(fragment)
        }

        val castedFragment = clazz.cast(fragment)!!
        val item = castedFragment.getCurrentItem()

        val position = mediator.getPositionByItemInPath(item)

        if (position == -1) {
            return true
        }

        return mediator.isDifferentPathCache(position)
    }

    override fun generate(position: Int): Fragment {

        val countPath = mediator.countPath()
        if (position >= countPath) {
            return next!!.generate(position - countPath)
        }

        val item = mediator.getItemByPositionInPath(position)

        val fragment = clazz.newInstance()

        fragment.setInitialData(item, mediator, if (countPath > position + 1) {
            mediator.getItemByPositionInPath(position + 1)
        } else {
            null
        })

        return fragment
    }

    override fun count(): Int {

        val countPath = mediator.countPath()

        if (!mediator.isNonFinalCache(mediator.getItemByPositionInPath(countPath - 1))) {
            return countPath + (next?.count() ?: 0)
        }

        return countPath
    }

}
