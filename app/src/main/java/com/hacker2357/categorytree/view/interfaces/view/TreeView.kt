package com.hacker2357.categorytree.view.interfaces.view

import com.hacker2357.domain.data.response.CategoryDataRes

interface TreeView : BaseView {
    fun setCategories(categoryId: Long?, categories: List<CategoryDataRes>)
}