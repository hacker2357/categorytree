package com.hacker2357.categorytree.view.component.implementation

import android.support.v7.widget.RecyclerView
import com.hacker2357.categorytree.view.component.interfaces.RecyclerViewScrollOffsetComputer

class RecyclerViewScrollOffsetComputerImpl: RecyclerViewScrollOffsetComputer {
    override fun compute(recyclerView: RecyclerView): Int {
        if (recyclerView.height == 0) {
            return 0
        }
        return (recyclerView.computeVerticalScrollOffset() + (recyclerView.height / 9 / 2)) / (recyclerView.height / 9)
    }
}