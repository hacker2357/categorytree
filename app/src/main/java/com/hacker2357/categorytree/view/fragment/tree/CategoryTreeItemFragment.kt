package com.hacker2357.categorytree.view.fragment.tree

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hacker2357.categorytree.App
import com.hacker2357.categorytree.R
import com.hacker2357.categorytree.dagger.component.CategoryTreeItemComponent
import com.hacker2357.categorytree.dagger.component.DaggerCategoryTreeItemComponent
import com.hacker2357.categorytree.view.adapter.CategoryListAdapter
import com.hacker2357.domain.data.response.CategoryDataRes
import javax.inject.Inject
import android.support.v7.widget.LinearSnapHelper
import android.util.Log
import com.hacker2357.categorytree.view.component.implementation.RecyclerViewScrollDetector
import com.hacker2357.categorytree.view.adapter.CategoryMediator2ScrollListenerAdapter
import com.hacker2357.categorytree.view.component.interfaces.RecyclerViewScrollOffsetComputer
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import com.hacker2357.categorytree.view.pattern.observer.interfaces.CustomBusListener
import kotlinx.android.synthetic.main.fragment_item_list_category.*


class CategoryTreeItemFragment : DataFragmentItemFragment<Long, CategoryDataRes>() {

    @Inject
    protected lateinit var adapter: CategoryListAdapter
    @Inject
    protected lateinit var recyclerViewScrollOffsetComputer: RecyclerViewScrollOffsetComputer

    private lateinit var categoryListComponent: CategoryTreeItemComponent

    private lateinit var _mediator: TreeMediator<Long, CategoryDataRes>
    private var currentItem: Long? = null
    private var activeItem: Long? = null

    override fun setInitialData(currentItem: Long?, mediator: TreeMediator<Long, CategoryDataRes>, activeItem: Long?) {

        this.currentItem = currentItem
        this.activeItem = activeItem
        _mediator = mediator

    }

    override fun getCurrentItem(): Long? {
        return currentItem
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_list_category, container, false)
    }


    override fun onActivate() {
        val position = recyclerViewScrollOffsetComputer.compute(recyclerView)
        if (adapter.itemCount - 8 > position) {
            val targetItem = adapter.getItem(position)

            val extractedId = _mediator.extractId(targetItem)
            _mediator.navigateTo(extractedId)
            setProgressBar(position, true)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        val list = _mediator.getChildrenFromCache(currentItem) ?: ArrayList()
        adapter.setData(list)

        recyclerView.addOnScrollListener(
                RecyclerViewScrollDetector(
                        CategoryMediator2ScrollListenerAdapter(adapter, _mediator, recyclerViewScrollOffsetComputer)
                )
        )

        _mediator.subscribeBus(object : CustomBusListener<Long?, List<CategoryDataRes>?> {
            override fun onEvent(request: Long?, data: List<CategoryDataRes>?) {

                val position = list.indexOf(_mediator.getCachedItemById(request))
                if (position != -1) {
                    setProgressBar(position, false)
                }


            }

            override fun onFailed(e: Throwable) {

            }

        })

        recyclerView.post {

            if (list.isNotEmpty()) {

                val item = activeItem ?: _mediator.extractId(list.first())

                val position = list.indexOf(list.find { it.id == item })

                if (currentItem === null) {
                    _mediator.navigateTo(item)
                    setProgressBar(position, true)
                }

                recyclerView.scrollToPosition(position)
                Log.e("RecyclerView Position", position.toString())
//                setProgressBar(position, true)
            }

        }

    }

    private fun setProgressBar(position: Int, state: Boolean) {
        if (adapter.itemCount > 0) {
            val pathTargetItem = _mediator.extractId(adapter.getItem(position))
            if (!state || !_mediator.isNonFinalCache(pathTargetItem)) {
                val holder = recyclerView?.findViewHolderForAdapterPosition(position + 4) as CategoryListAdapter.ViewHolder?
                holder?.showProgressBar(state)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        categoryListComponent = DaggerCategoryTreeItemComponent.builder()
                .mainComponent(App.mainComponent())
                .build()

        categoryListComponent.inject(this)
    }
}