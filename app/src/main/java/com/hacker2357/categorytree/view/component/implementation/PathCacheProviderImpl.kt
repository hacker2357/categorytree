package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.PathCacheProvider
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator

class PathCacheProviderImpl<RequestDataType: Comparable<RequestDataType>, ReturnItemDataType> : PathCacheProvider<RequestDataType, ReturnItemDataType> {

    private val _path: MutableList<RequestDataType?> = arrayListOf()

    private lateinit var _mediator: TreeMediator<RequestDataType, ReturnItemDataType>


    override fun clonePath() {
        _path.clear()
        _path.addAll(_mediator.getPath())
    }

    override fun isDifferent(position: Int): Boolean {

        if (position >= _path.size) {
            return true
        }

        val item = _mediator.getItemByPositionInPath(position)

        return _path[position] != item
    }

    override fun setMediator(mediator: TreeMediator<RequestDataType, ReturnItemDataType>) {
        _mediator = mediator
    }

    override fun clear() {
        _path.clear()
    }
}