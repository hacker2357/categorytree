package com.hacker2357.categorytree.view.component.implementation

import com.hacker2357.categorytree.view.component.interfaces.TreeCacheProvider
import com.hacker2357.categorytree.view.component.interfaces.TreeMediator
import io.reactivex.Observable
import java.util.*
import kotlin.collections.ArrayList

open class TreeCacheProviderImpl<RequestDataType: Comparable<RequestDataType>, ReturnItemType> : TreeCacheProvider<RequestDataType, ReturnItemType> {

    private val cache: SortedSet<ReturnItemType> = sortedSetOf(Comparator { o1, o2 ->
        when {
            _mediator.extractId(o1) == _mediator.extractId(o2) -> 0
            _mediator.extractId(o1) > _mediator.extractId(o2) -> 1
            else -> -1
        }
    })

    private lateinit var _mediator: TreeMediator<RequestDataType, ReturnItemType>

    override fun setMediator(mediator: TreeMediator<RequestDataType, ReturnItemType>) {
        _mediator = mediator
    }

    override fun getChildren(item: RequestDataType?): List<ReturnItemType>? {

        return Observable.fromIterable(cache)
                .filter { if (item !== null) {
                            item == _mediator.extractParent(it)
                        } else {
                        null === _mediator.extractParent(it)
                        }
                }
                .blockingIterable()
                .toList()
    }

    override fun getPeers(item: RequestDataType): List<ReturnItemType> {
        return Observable.fromIterable(cache)
                .filter { _mediator.extractRequestParent(item) == _mediator.extractParent(it) }
                .blockingIterable()
                .toList()

    }

    override fun addList(newList: List<ReturnItemType>) {
        cache.addAll(newList)
    }

    override fun getById(itemId: RequestDataType?): ReturnItemType? {
        val blockingIterable = Observable.fromIterable(cache)
                .filter { itemId == _mediator.extractId(it) }
                .blockingIterable()

        if (blockingIterable.toList().isEmpty()) {
            return null
        }

        return blockingIterable
                .iterator()
                .next()
    }

    override fun getParent(item: RequestDataType): ReturnItemType? {
        val blockingIterable = Observable.fromIterable(cache)
                .filter { item == _mediator.extractId(it) }
                .blockingIterable()

        if (blockingIterable.toList().isEmpty()) {
            return null
        }

        return getById(_mediator.extractParent(blockingIterable
                .iterator()
                .next()))
    }

    override fun isNonFinal(item: RequestDataType?): Boolean {

        if (item === null) {
            return false
        }

        if (ArrayList(cache).any { _mediator.extractParent(it) == item }) {
            return true
        }

        return false
    }
}
