package com.hacker2357.categorytree

import android.app.Application
import com.hacker2357.categorytree.dagger.component.DaggerMainComponent
import com.hacker2357.categorytree.dagger.component.MainComponent
import com.hacker2357.categorytree.dagger.module.main.AppModule

class App : Application() {
    companion object {
        private lateinit var app: App
        private lateinit var mainComponent: MainComponent

        fun appContext() = app
        fun mainComponent(): MainComponent = mainComponent
    }

    override fun onCreate() {
        super.onCreate()
        app = this

        mainComponent = DaggerMainComponent.builder()
                .appModule(AppModule(this.applicationContext))
                .build()
    }
}