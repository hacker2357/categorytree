package com.hacker2357.domain.data.response

data class CategoryDataRes(val id: Long, val name: String, val parent: Long? = null)